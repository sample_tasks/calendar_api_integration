"use client";
import { ACCESS_TOKEN } from "@/constants/ENVIRONMENT_VARIABLES";
import {
  GET_ACTIVITIES_API,
  GET_CANDIDATE_STATUS_API,
  GET_HIRED_API,
  GET_INTERVIEW_AND_HIRED_DETAILS_API,
  GET_POSTED_JOB_Active_LISTS_API,
  GET_POSTED_JOB_LISTS_API,
  GET_TICKET_LIST_API,
  GET_TODAY_MEETING_DETAILS_API,
  GET_UPCOMINGS_API,
  // INVENTORY_ASSETS_API,
  LOGIN_API,
  LOGOUT_API,
  // NOTIFICATIONS_API,
  USER_ACCOUNT_MANAGEMENT_ACCOUNT_API,
  GET_MEETING_LIST,
  GET_MEETING_INFO_TODAY
} from "@/utils/API";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// @ts-ignore
import Cookies from "js-cookie"; // Import the js-cookie library

// Helper function to handle common async thunk logic
const createAsyncThunkHandler = (apiFunction: any, propName: any, onLoginSuccess?: any) =>
  createAsyncThunk(`dashboard/${propName}`, async (data: any) => {
    try {
      // Call the API function with provided data
      let response;
      if (data) {
        response = await apiFunction(data);
      } else {
        response = await apiFunction();
      }

      // If there's a login success callback, call it with the response
      if (onLoginSuccess && propName === "login") {
        onLoginSuccess(response);
      }

      return response;
    } catch (error) {
      // Handle error
      // console.log("propsname = ", propName);
      if (propName === "login") {
        // onLoginSuccess({
        //   "user_id": 8,
        //   "user_email": "consultant_senthilnathan@dataterrain.com",
        //   "user_role": "dev_employee",
        //   "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzE4Mzg3OTMzLCJpYXQiOjE3MTgzMDE1MzMsImp0aSI6IjQyMjZiMWQ2MDRiNTRiNTZhNTQzNTVhNDk1MGQxYmI1IiwidXNlcl9pZCI6OH0.B7VSO2fOs6RKr_u6SJZOCDMgrB6_kWX48e1Q6qaVJy8",
        //   "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTcxOTU5NzUzMywiaWF0IjoxNzE4MzAxNTMzLCJqdGkiOiJlYjlkZTlkZjRiNTY0ZmI2YjQyNGU2YmRhNTY2OTc1MSIsInVzZXJfaWQiOjh9.CnL0gRv3KQzKpvJFNa8NvM3ZSlGf0kM8PkBGdAlOBfA"
        // });
        // onLoginSuccess({
        //   "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        //   // "access_token": "",
        //   "refresh_token": "dGhpcyBpcyBhIHNhbXBsZSBzZWNyZXQgdG9rZW4gdGhhdCBpcyBsb25nZXIgdGhhbiBhbiBhY2Nlc3MgdG9rZW4gYW5kIGlzIHVzZWQgdG8gZ2VuZXJhdGUgbmV3IGFjY2VzcyB0b2tlbnMgd2hlbiB0aGUgY3VycmVudCBvbmVzIGV4cGlyZXMuIFRoaXMgaXMganVzdCBhIHNhbXBsZSB0b2tlbi4",
        //   // "refresh_token": "",
        //   "user_email": "consultant_senthilnathan@dataterrain.com",
        //   "user_id": "f47ac10b-58cc-4372-a567-0e02b2c3d479"
        // });
      }
      // if (propName == "get_meetings_list") {
      //   return ([
      //     {
      //       "id": 1,
      //       "summary": "test senthil",
      //       "desc": "test senthil test senthil",
      //       "start": "2024-06-04T17:00:00+05:30",
      //       "end": "2024-06-04T19:00:00+05:30",
      //       "attendees": "['pmariraj@dataterrain.com', 'yogeshwari@dataterrain.com', 'mohanrajk@dataterrain.com', 'vinodhini_hr@dataterrain.com']",
      //       "status": null,
      //       "comment": null,
      //       "score": {
      //         "sf": 3
      //       },
      //       "link": "https://meet.google.com/qwe-rtyu-iop",
      //       "user_det": {
      //         "id": 2,
      //         "question_score": null,
      //         "status": null,
      //         "candidate": {
      //           "id": 9,
      //           "candidate_firstName": "yogeshwari",
      //           "candidate_lastName": "yogi",
      //           "candidateGender": "female",
      //           "candidateComment": "",
      //           "candidate_email": "yogeshwari@dataterrain.com",
      //           "candidate_dob": "2001-06-06T18:24:47+05:30",
      //           "candidate_phoneNumber": "+12125552368",
      //           "candidateAppliedDate": "2024-06-06T18:24:50+05:30",
      //           "candidate_isActive": true,
      //           "candidate_keySkill_Experience": {
      //             "html": 4,
      //             "python": 4
      //           },
      //           "candidate_yoe": null,
      //           "candidateOtherSkills": {
      //             "html": 4,
      //             "python": 4
      //           },
      //           "candidateQualification": "B.E",
      //           "candidateWorkMode": "onsite",
      //           "candidate_noticePeriod": "30",
      //           "candidate_location": "madurai",
      //           "candidateDistance": "400",
      //           "candidateTotalExperience": null,
      //           "candidate_onNotice": null,
      //           "candidate_currentSalary": 0,
      //           "candidate_expectedSalary": 0,
      //           "candidate_resume": "/media/uploads/Boopathy_S_Resume_FLZuhkl.pdf",
      //           "candidate_IsWillingToMove": false,
      //           "candidate_resume_modification": null,
      //           "candidateProfileImage": "/media/static/images/candidate_avatar_default.svg"
      //         },
      //         "handled_by": {
      //           "id": 3,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "vinodhini_hr@dataterrain.com",
      //           "username": "vinodhinihr",
      //           "firstName": "Vinodhini HR",
      //           "lastName": "vino",
      //           "userRole": "hr_employee",
      //           "userPhoneNumber": "",
      //           "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-12T16:30:13.263459+05:30",
      //           "userdateofbirth": "2024-06-11T00:00:00+05:30",
      //           "userbloodgroup": "o+ve",
      //           "userdescription": null,
      //           "usercity": "Madurai",
      //           "userstate": "tamil nadu",
      //           "userzipcode": "600089",
      //           "usercountry": "india",
      //           "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         },
      //         "job_id": {
      //           "id": 16,
      //           "jobRequest_Title": "UI Designer",
      //           "jobRequest_Role": "Senior Software Engineer",
      //           "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //           "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //           "jobRequest_Expected_YOE": "3 to 5",
      //           "jobRequest_TotalVacancy": 1,
      //           "jobRequest_FilledVacancy": 0,
      //           "jobRequest_isActive": true,
      //           "jobRequest_isClosed": false,
      //           "jobRequest_has_Started": true,
      //           "jobRequest_isPriority": false,
      //           "jobRequest_PriorityValue": 0,
      //           "jobRequest_MoveToAssets": false,
      //           "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //           "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //           "jobRequest_createdBy": 4
      //         }
      //       },
      //       "job_id": {
      //         "id": 21,
      //         "jobRequest_Title": "Django",
      //         "jobRequest_Role": "Django",
      //         "jobRequest_KeySkills": "front_end javascript python backend pdf web_technologies application_programming django oops",
      //         "jobRequest_Description": "Job Description goes here",
      //         "jobRequest_Expected_YOE": "0 to 3 years",
      //         "jobRequest_TotalVacancy": 10,
      //         "jobRequest_FilledVacancy": 0,
      //         "jobRequest_isActive": true,
      //         "jobRequest_isClosed": false,
      //         "jobRequest_has_Started": false,
      //         "jobRequest_isPriority": true,
      //         "jobRequest_PriorityValue": 0,
      //         "jobRequest_MoveToAssets": false,
      //         "jobRequest_createdOn": "2024-06-06T21:49:01.864491+05:30",
      //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //         "jobRequest_createdBy": {
      //           "id": 4,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "dilli_project_manager@dataterrain.com",
      //           "username": "dillipm",
      //           "firstName": "Dilli HR",
      //           "lastName": "Rathnam",
      //           "userRole": "project_manager",
      //           "userPhoneNumber": "+12125552368",
      //           "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-17T14:15:09.711185+05:30",
      //           "userdateofbirth": null,
      //           "userbloodgroup": null,
      //           "userdescription": null,
      //           "usercity": "chennai",
      //           "userstate": null,
      //           "userzipcode": "600099",
      //           "usercountry": null,
      //           "userProfileImage": "/media/static/images/employee_avatar_default.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         }
      //       }
      //     },
      //     {
      //       "id": 2,
      //       "summary": "TEST SUMMARY",
      //       "desc": "Third Level Backend Interview",
      //       "start": "2024-06-06T17:09:03+05:30",
      //       "end": "2024-06-06T17:09:03+05:30",
      //       "attendees": "['attendee_1@gmail.com', 'attendee_2@gmail.com', 'attendee_3@gmail.com']",
      //       "status": null,
      //       "comment": null,
      //       "score": {
      //         "html": 10
      //       },
      //       "link": "https://meet.google.com/opq-rstu-vwx",
      //       "user_det": {
      //         "id": 3,
      //         "question_score": null,
      //         "status": null,
      //         "candidate": {
      //           "id": 8,
      //           "candidate_firstName": "Praveen",
      //           "candidate_lastName": "Kumar",
      //           "candidateGender": "male",
      //           "candidateComment": "",
      //           "candidate_email": "pmariraj@dataterrain.com",
      //           "candidate_dob": "1997-06-06T18:23:07+05:30",
      //           "candidate_phoneNumber": "+12125552344",
      //           "candidateAppliedDate": "2024-06-06T18:23:11+05:30",
      //           "candidate_isActive": true,
      //           "candidate_keySkill_Experience": {
      //             "Machine learning": 2,
      //             "python": 2
      //           },
      //           "candidate_yoe": null,
      //           "candidateOtherSkills": {
      //             "Machine learning": 2,
      //             "python": 2
      //           },
      //           "candidateQualification": "B.E",
      //           "candidateWorkMode": "work from home",
      //           "candidate_noticePeriod": "30",
      //           "candidate_location": "dubai",
      //           "candidateDistance": "0",
      //           "candidateTotalExperience": null,
      //           "candidate_onNotice": null,
      //           "candidate_currentSalary": 0,
      //           "candidate_expectedSalary": 0,
      //           "candidate_resume": "/media/uploads/Boopathy_S_Resume_nxarlOJ.pdf",
      //           "candidate_IsWillingToMove": false,
      //           "candidate_resume_modification": null,
      //           "candidateProfileImage": "/media/static/images/candidate_avatar_default.svg"
      //         },
      //         "handled_by": {
      //           "id": 3,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "vinodhini_hr@dataterrain.com",
      //           "username": "vinodhinihr",
      //           "firstName": "Vinodhini HR",
      //           "lastName": "vino",
      //           "userRole": "hr_employee",
      //           "userPhoneNumber": "",
      //           "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-12T16:30:13.263459+05:30",
      //           "userdateofbirth": "2024-06-11T00:00:00+05:30",
      //           "userbloodgroup": "o+ve",
      //           "userdescription": null,
      //           "usercity": "Madurai",
      //           "userstate": "tamil nadu",
      //           "userzipcode": "600089",
      //           "usercountry": "india",
      //           "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         },
      //         "job_id": {
      //           "id": 16,
      //           "jobRequest_Title": "UI Designer",
      //           "jobRequest_Role": "Senior Software Engineer",
      //           "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //           "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //           "jobRequest_Expected_YOE": "3 to 5",
      //           "jobRequest_TotalVacancy": 1,
      //           "jobRequest_FilledVacancy": 0,
      //           "jobRequest_isActive": true,
      //           "jobRequest_isClosed": false,
      //           "jobRequest_has_Started": true,
      //           "jobRequest_isPriority": false,
      //           "jobRequest_PriorityValue": 0,
      //           "jobRequest_MoveToAssets": false,
      //           "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //           "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //           "jobRequest_createdBy": 4
      //         }
      //       },
      //       "job_id": {
      //         "id": 16,
      //         "jobRequest_Title": "UI Designer",
      //         "jobRequest_Role": "Senior Software Engineer",
      //         "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //         "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //         "jobRequest_Expected_YOE": "3 to 5",
      //         "jobRequest_TotalVacancy": 1,
      //         "jobRequest_FilledVacancy": 0,
      //         "jobRequest_isActive": true,
      //         "jobRequest_isClosed": false,
      //         "jobRequest_has_Started": true,
      //         "jobRequest_isPriority": false,
      //         "jobRequest_PriorityValue": 0,
      //         "jobRequest_MoveToAssets": false,
      //         "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //         "jobRequest_createdBy": {
      //           "id": 4,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "dilli_project_manager@dataterrain.com",
      //           "username": "dillipm",
      //           "firstName": "Dilli HR",
      //           "lastName": "Rathnam",
      //           "userRole": "project_manager",
      //           "userPhoneNumber": "+12125552368",
      //           "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-17T14:15:09.711185+05:30",
      //           "userdateofbirth": null,
      //           "userbloodgroup": null,
      //           "userdescription": null,
      //           "usercity": "chennai",
      //           "userstate": null,
      //           "userzipcode": "600099",
      //           "usercountry": null,
      //           "userProfileImage": "/media/static/images/employee_avatar_default.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         }
      //       }
      //     },
      //     {
      //       "id": 3,
      //       "summary": "First Level Interview",
      //       "desc": "First Level Interview",
      //       "start": "2024-06-06T23:43:56+05:30",
      //       "end": "2024-06-06T23:50:54+05:30",
      //       "attendees": "[\"praveen@dataterrain.com\", \"arun@dataterrain.com]",
      //       "status": null,
      //       "comment": "good performance in first level",
      //       "score": {
      //         "python": 10
      //       },
      //       "link": "https://meet.google.com/tac-gkjx-yhn",
      //       "user_det": {
      //         "id": 1,
      //         "question_score": 1,
      //         "status": null,
      //         "candidate": {
      //           "id": 7,
      //           "candidate_firstName": "Mohan",
      //           "candidate_lastName": "raj",
      //           "candidateGender": "male",
      //           "candidateComment": "",
      //           "candidate_email": "mohanrajk@dataterrain.com",
      //           "candidate_dob": "2024-12-10T18:20:54+05:30",
      //           "candidate_phoneNumber": "+12125552317",
      //           "candidateAppliedDate": "2024-06-06T18:20:57+05:30",
      //           "candidate_isActive": true,
      //           "candidate_keySkill_Experience": {
      //             "Machine learning": 2,
      //             "python": 2
      //           },
      //           "candidate_yoe": null,
      //           "candidateOtherSkills": {
      //             "Machine learning": 2,
      //             "python": 2
      //           },
      //           "candidateQualification": "B.E",
      //           "candidateWorkMode": "onsite",
      //           "candidate_noticePeriod": "30",
      //           "candidate_location": "Chennai",
      //           "candidateDistance": "0",
      //           "candidateTotalExperience": null,
      //           "candidate_onNotice": null,
      //           "candidate_currentSalary": 0,
      //           "candidate_expectedSalary": 0,
      //           "candidate_resume": null,
      //           "candidate_IsWillingToMove": false,
      //           "candidate_resume_modification": null,
      //           "candidateProfileImage": "/media/static/cand_profile_images/Karthik_Resume_1.pdf"
      //         },
      //         "handled_by": {
      //           "id": 3,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "vinodhini_hr@dataterrain.com",
      //           "username": "vinodhinihr",
      //           "firstName": "Vinodhini HR",
      //           "lastName": "vino",
      //           "userRole": "hr_employee",
      //           "userPhoneNumber": "",
      //           "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-12T16:30:13.263459+05:30",
      //           "userdateofbirth": "2024-06-11T00:00:00+05:30",
      //           "userbloodgroup": "o+ve",
      //           "userdescription": null,
      //           "usercity": "Madurai",
      //           "userstate": "tamil nadu",
      //           "userzipcode": "600089",
      //           "usercountry": "india",
      //           "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         },
      //         "job_id": {
      //           "id": 16,
      //           "jobRequest_Title": "UI Designer",
      //           "jobRequest_Role": "Senior Software Engineer",
      //           "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //           "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //           "jobRequest_Expected_YOE": "3 to 5",
      //           "jobRequest_TotalVacancy": 1,
      //           "jobRequest_FilledVacancy": 0,
      //           "jobRequest_isActive": true,
      //           "jobRequest_isClosed": false,
      //           "jobRequest_has_Started": true,
      //           "jobRequest_isPriority": false,
      //           "jobRequest_PriorityValue": 0,
      //           "jobRequest_MoveToAssets": false,
      //           "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //           "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //           "jobRequest_createdBy": 4
      //         }
      //       },
      //       "job_id": {
      //         "id": 16,
      //         "jobRequest_Title": "UI Designer",
      //         "jobRequest_Role": "Senior Software Engineer",
      //         "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //         "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //         "jobRequest_Expected_YOE": "3 to 5",
      //         "jobRequest_TotalVacancy": 1,
      //         "jobRequest_FilledVacancy": 0,
      //         "jobRequest_isActive": true,
      //         "jobRequest_isClosed": false,
      //         "jobRequest_has_Started": true,
      //         "jobRequest_isPriority": false,
      //         "jobRequest_PriorityValue": 0,
      //         "jobRequest_MoveToAssets": false,
      //         "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //         "jobRequest_createdBy": {
      //           "id": 4,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "dilli_project_manager@dataterrain.com",
      //           "username": "dillipm",
      //           "firstName": "Dilli HR",
      //           "lastName": "Rathnam",
      //           "userRole": "project_manager",
      //           "userPhoneNumber": "+12125552368",
      //           "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-17T14:15:09.711185+05:30",
      //           "userdateofbirth": null,
      //           "userbloodgroup": null,
      //           "userdescription": null,
      //           "usercity": "chennai",
      //           "userstate": null,
      //           "userzipcode": "600099",
      //           "usercountry": null,
      //           "userProfileImage": "/media/static/images/employee_avatar_default.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         }
      //       }
      //     },
      //     {
      //       "id": 4,
      //       "summary": "First level cloud interview",
      //       "desc": "First Level Interview",
      //       "start": "2024-06-04T17:00:00+05:30",
      //       "end": "2024-06-04T18:00:00+05:30",
      //       "attendees": "[\"test_1@gmail.com\", \"test_2@gmail.com\"]",
      //       "status": null,
      //       "comment": null,
      //       "score": {
      //         "django": 10
      //       },
      //       "link": "https://meet.google.com/tac-gkjx-yhn",
      //       "user_det": {
      //         "id": 2,
      //         "question_score": null,
      //         "status": null,
      //         "candidate": {
      //           "id": 9,
      //           "candidate_firstName": "yogeshwari",
      //           "candidate_lastName": "yogi",
      //           "candidateGender": "female",
      //           "candidateComment": "",
      //           "candidate_email": "yogeshwari@dataterrain.com",
      //           "candidate_dob": "2001-06-06T18:24:47+05:30",
      //           "candidate_phoneNumber": "+12125552368",
      //           "candidateAppliedDate": "2024-06-06T18:24:50+05:30",
      //           "candidate_isActive": true,
      //           "candidate_keySkill_Experience": {
      //             "html": 4,
      //             "python": 4
      //           },
      //           "candidate_yoe": null,
      //           "candidateOtherSkills": {
      //             "html": 4,
      //             "python": 4
      //           },
      //           "candidateQualification": "B.E",
      //           "candidateWorkMode": "onsite",
      //           "candidate_noticePeriod": "30",
      //           "candidate_location": "madurai",
      //           "candidateDistance": "400",
      //           "candidateTotalExperience": null,
      //           "candidate_onNotice": null,
      //           "candidate_currentSalary": 0,
      //           "candidate_expectedSalary": 0,
      //           "candidate_resume": "/media/uploads/Boopathy_S_Resume_FLZuhkl.pdf",
      //           "candidate_IsWillingToMove": false,
      //           "candidate_resume_modification": null,
      //           "candidateProfileImage": "/media/static/images/candidate_avatar_default.svg"
      //         },
      //         "handled_by": {
      //           "id": 3,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "vinodhini_hr@dataterrain.com",
      //           "username": "vinodhinihr",
      //           "firstName": "Vinodhini HR",
      //           "lastName": "vino",
      //           "userRole": "hr_employee",
      //           "userPhoneNumber": "",
      //           "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-12T16:30:13.263459+05:30",
      //           "userdateofbirth": "2024-06-11T00:00:00+05:30",
      //           "userbloodgroup": "o+ve",
      //           "userdescription": null,
      //           "usercity": "Madurai",
      //           "userstate": "tamil nadu",
      //           "userzipcode": "600089",
      //           "usercountry": "india",
      //           "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         },
      //         "job_id": {
      //           "id": 16,
      //           "jobRequest_Title": "UI Designer",
      //           "jobRequest_Role": "Senior Software Engineer",
      //           "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //           "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //           "jobRequest_Expected_YOE": "3 to 5",
      //           "jobRequest_TotalVacancy": 1,
      //           "jobRequest_FilledVacancy": 0,
      //           "jobRequest_isActive": true,
      //           "jobRequest_isClosed": false,
      //           "jobRequest_has_Started": true,
      //           "jobRequest_isPriority": false,
      //           "jobRequest_PriorityValue": 0,
      //           "jobRequest_MoveToAssets": false,
      //           "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //           "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //           "jobRequest_createdBy": 4
      //         }
      //       },
      //       "job_id": {
      //         "id": 16,
      //         "jobRequest_Title": "UI Designer",
      //         "jobRequest_Role": "Senior Software Engineer",
      //         "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //         "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //         "jobRequest_Expected_YOE": "3 to 5",
      //         "jobRequest_TotalVacancy": 1,
      //         "jobRequest_FilledVacancy": 0,
      //         "jobRequest_isActive": true,
      //         "jobRequest_isClosed": false,
      //         "jobRequest_has_Started": true,
      //         "jobRequest_isPriority": false,
      //         "jobRequest_PriorityValue": 0,
      //         "jobRequest_MoveToAssets": false,
      //         "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //         "jobRequest_createdBy": {
      //           "id": 4,
      //           "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //           "last_login": null,
      //           "userEmail": "dilli_project_manager@dataterrain.com",
      //           "username": "dillipm",
      //           "firstName": "Dilli HR",
      //           "lastName": "Rathnam",
      //           "userRole": "project_manager",
      //           "userPhoneNumber": "+12125552368",
      //           "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
      //           "userinActiveDate": null,
      //           "userModifiedDate": "2024-06-17T14:15:09.711185+05:30",
      //           "userdateofbirth": null,
      //           "userbloodgroup": null,
      //           "userdescription": null,
      //           "usercity": "chennai",
      //           "userstate": null,
      //           "userzipcode": "600099",
      //           "usercountry": null,
      //           "userProfileImage": "/media/static/images/employee_avatar_default.svg",
      //           "is_staff": false,
      //           "is_active": true,
      //           "is_superuser": false,
      //           "groups": [],
      //           "user_permissions": []
      //         }
      //       }
      //     },

      //   ])
      // }
      // if (propName == "get_meetings_info") {
      //   return ({
      //     "id": 3,
      //     "summary": "First level cloud interview",
      //     "desc": "First Level Interview",
      //     "start": "2024-06-09T23:00:00+05:30",
      //     "end": "2024-06-09T23:30:00+05:30",
      //     "attendees": "[\"test_1@gmail.com\", \"test_2@gmail.com\"]",
      //     "status": null,
      //     "comment": null,
      //     "score": {
      //       "django": 10
      //     },
      //     "link": "https://meet.google.com/tac-gkjx-yhn",
      //     "user_det": {
      //       "id": 2,
      //       "question_score": null,
      //       "status": null,
      //       "candidate": {
      //         "id": 9,
      //         "candidate_firstName": "yogeshwari",
      //         "candidate_lastName": "yogi",
      //         "candidateGender": "female",
      //         "candidateComment": "",
      //         "candidate_email": "yogeshwari@dataterrain.com",
      //         "candidate_dob": "2001-06-06T18:24:47+05:30",
      //         "candidate_phoneNumber": "+12125552368",
      //         "candidateAppliedDate": "2024-06-06T18:24:50+05:30",
      //         "candidate_isActive": true,
      //         "candidate_keySkill_Experience": {
      //           "html": 4,
      //           "python": 4
      //         },
      //         "candidate_yoe": null,
      //         "candidateOtherSkills": {
      //           "html": 4,
      //           "python": 4
      //         },
      //         "candidateQualification": "B.E",
      //         "candidateWorkMode": "onsite",
      //         "candidate_noticePeriod": "30",
      //         "candidate_location": "madurai",
      //         "candidateDistance": "400",
      //         "candidateTotalExperience": null,
      //         "candidate_onNotice": null,
      //         "candidate_currentSalary": 0,
      //         "candidate_expectedSalary": 0,
      //         "candidate_resume": "/media/uploads/Boopathy_S_Resume_FLZuhkl.pdf",
      //         "candidate_IsWillingToMove": false,
      //         "candidate_resume_modification": null,
      //         "candidateProfileImage": "/media/static/images/candidate_avatar_default.svg"
      //       },
      //       "handled_by": {
      //         "id": 3,
      //         "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //         "last_login": null,
      //         "userEmail": "vinodhini_hr@dataterrain.com",
      //         "username": "vinodhinihr",
      //         "firstName": "Vinodhini HR",
      //         "lastName": "vino",
      //         "userRole": "hr_employee",
      //         "userPhoneNumber": "",
      //         "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
      //         "userinActiveDate": null,
      //         "userModifiedDate": "2024-06-14T11:19:33.439586+05:30",
      //         "userdateofbirth": "2024-06-11T00:00:00+05:30",
      //         "userbloodgroup": "o+ve",
      //         "userdescription": null,
      //         "usercity": "Madurai",
      //         "userstate": "tamil nadu",
      //         "userzipcode": "600089",
      //         "usercountry": "india",
      //         "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
      //         "is_staff": false,
      //         "is_active": true,
      //         "is_superuser": false,
      //         "groups": [],
      //         "user_permissions": []
      //       },
      //       "job_id": {
      //         "id": 16,
      //         "jobRequest_Title": "UI Designer",
      //         "jobRequest_Role": "Senior Software Engineer",
      //         "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //         "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //         "jobRequest_Expected_YOE": "3 to 5",
      //         "jobRequest_TotalVacancy": 1,
      //         "jobRequest_FilledVacancy": 0,
      //         "jobRequest_isActive": true,
      //         "jobRequest_isClosed": false,
      //         "jobRequest_has_Started": true,
      //         "jobRequest_isPriority": false,
      //         "jobRequest_PriorityValue": 0,
      //         "jobRequest_MoveToAssets": false,
      //         "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //         "jobRequest_createdBy": 4
      //       }
      //     },
      //     "job_id": {
      //       "id": 16,
      //       "jobRequest_Title": "UI Designer",
      //       "jobRequest_Role": "Senior Software Engineer",
      //       "jobRequest_KeySkills": "figma, html, css , js, adobe",
      //       "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
      //       "jobRequest_Expected_YOE": "3 to 5",
      //       "jobRequest_TotalVacancy": 1,
      //       "jobRequest_FilledVacancy": 0,
      //       "jobRequest_isActive": true,
      //       "jobRequest_isClosed": false,
      //       "jobRequest_has_Started": true,
      //       "jobRequest_isPriority": false,
      //       "jobRequest_PriorityValue": 0,
      //       "jobRequest_MoveToAssets": false,
      //       "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
      //       "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
      //       "jobRequest_createdBy": {
      //         "id": 4,
      //         "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
      //         "last_login": null,
      //         "userEmail": "dilli_project_manager@dataterrain.com",
      //         "username": "dillipm",
      //         "firstName": "Dilli HR",
      //         "lastName": "Rathnam",
      //         "userRole": "project_manager",
      //         "userPhoneNumber": "+12125552368",
      //         "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
      //         "userinActiveDate": null,
      //         "userModifiedDate": "2024-06-12T19:57:06.631331+05:30",
      //         "userdateofbirth": null,
      //         "userbloodgroup": null,
      //         "userdescription": null,
      //         "usercity": "chennai",
      //         "userstate": null,
      //         "userzipcode": "600099",
      //         "usercountry": null,
      //         "userProfileImage": "/media/static/images/employee_avatar_default.svg",
      //         "is_staff": false,
      //         "is_active": true,
      //         "is_superuser": false,
      //         "groups": [],
      //         "user_permissions": []
      //       }
      //     }
      //   }
      //   )
      // }
      console.error(`${propName} failed:`, error);
      throw error;
    }
  });

// Define async thunks using the helper function
export const getCandidateStatusList = createAsyncThunkHandler(
  GET_CANDIDATE_STATUS_API,
  "candidate_status_list"
);

export const getInterviewAndHiredDetails = createAsyncThunkHandler(
  GET_INTERVIEW_AND_HIRED_DETAILS_API,
  "interview_and_hired_details"
);

export const getPostedJobList = createAsyncThunkHandler(
  GET_POSTED_JOB_LISTS_API,
  "posted_job_list"
);

export const getPostedJobActiveList = createAsyncThunkHandler(
  GET_POSTED_JOB_Active_LISTS_API,
  "posted_job_active_list"
);

export const getTodayMeetingDetailsList = createAsyncThunkHandler(
  GET_TODAY_MEETING_DETAILS_API,
  "today_meeting_details_list"
);

export const getActivities = createAsyncThunkHandler(
  GET_ACTIVITIES_API,
  "activities_list"
);

export const getUpcomings = createAsyncThunkHandler(
  GET_UPCOMINGS_API,
  "upcomings_list"
);

export const getHirings = createAsyncThunkHandler(
  GET_HIRED_API,
  "hirings_list"
);
export const userlogin = createAsyncThunkHandler(
  LOGIN_API,
  "login",
  (response: any) => {
    // Assuming response contains session information such as user data, access token, etc.
    const { access_token, user_email, user_id, refresh_token } = response;
    // Store user data and access token in cookies
    // Cookies.remove(ACCESS_TOKEN);
    Cookies.set(
      ACCESS_TOKEN,
      JSON.stringify({ access_token, user_email, user_id, refresh_token })
    );
  }
);

export const getTicketList = createAsyncThunkHandler(
  GET_TICKET_LIST_API,
  "ticket_List"
);

export const userLogout = createAsyncThunkHandler(LOGOUT_API, "logout");

export const userAccountManagementAccount = createAsyncThunkHandler(
  USER_ACCOUNT_MANAGEMENT_ACCOUNT_API,
  "user_account_management"
);

export const getMeetingsList = createAsyncThunkHandler(
  GET_MEETING_LIST,
  "get_meetings_list"
);

export const getMeetingInfo = createAsyncThunkHandler(
  GET_MEETING_INFO_TODAY,
  "get_meetings_info"
);

// export const inventoryAssets = createAsyncThunkHandler(
//   INVENTORY_ASSETS_API,
//   "inventory_Assets"
// );

// export const getNotifications = createAsyncThunkHandler(
//   NOTIFICATIONS_API,
//   "notification_list"
// );
const dashboardSlice = createSlice({
  name: "dashboard",
  initialState: {
    candidate_status_list: [],
    interview_and_hired_details: {},
    posted_job_list: [],
    posted_job_active_list: [],
    today_meeting_details_list: [],
    activities_list: [],
    upcomings_list: [],
    hirings_list: [],
    ticket_List: [],
    inventory_Assets: [],
    notification_list: [],
    login: {},
    logout: {},
    user_account_management: {},
    loading: false,
    calendarList: []
  },
  reducers: {
    clearData: (state, action) => {
      // @ts-ignore
      state.get_meetings_info = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(
        (action) =>
          action.type.startsWith("dashboard/") &&
          action.type.endsWith("/pending"),
        (state) => {
          state.loading = true;
        }
      )
      .addMatcher(
        (action) =>
          action.type.startsWith("dashboard/") &&
          action.type.endsWith("/fulfilled"),
        (state, action) => {
          // @ts-ignore
          // console.log("action = ", action.type, "payload = ", action?.payload);
          state.loading = false;
          const propName = action.type.split("/")[1]; // Extract property name from action type
          if (propName == "get_meetings_info") {
            // @ts-ignore
            const obj = action.payload;
            const detailView = {
              id: obj?.id,
              title: obj?.user_det && obj?.user_det?.job_id && obj?.user_det?.job_id?.jobRequest_Title,
              start: new Date(obj?.start),
              end: new Date(obj?.end),
              role: obj?.user_det && obj?.user_det?.job_id && obj?.user_det?.job_id?.jobRequest_Title || "",
              interviewer: obj?.user_det && obj?.user_det?.handled_by && `${(obj?.user_det?.handled_by?.firstName || "") + (obj?.user_det?.handled_by?.lastName || "")}` || "",
              time: obj?.start,
              detailView: {
                interviewWith: obj?.user_det && obj?.user_det?.candidate && `${(obj?.user_det?.candidate?.candidate_firstName || "") + (obj?.user_det?.candidate?.candidate_lastName || "")}` || "",
                position: obj?.job_id && obj?.job_id?.jobRequest_Title || "",
                created_by: obj?.job_id?.jobRequest_createdBy && obj?.job_id?.jobRequest_createdBy?.userRole || "",
                interview_date: obj?.start,
                interview_time: obj?.end,
                interview_via: "Google Meet"
              }
            }
            // @ts-ignore
            // console.log("entered2 = ", action.type, "payload = ", action.payload);
            // @ts-ignore
            state[propName] = detailView;
          } else {
            // @ts-ignore

            // @ts-ignore
            state[propName] = action.payload;
          }
        }
      )
      .addMatcher(
        (action) =>
          action.type.startsWith("dashboard/") &&
          action.type.endsWith("/rejected"),
        (state, action) => {
          // console.log("entered on reject = ", action.type);
          state.loading = false;
          // const propName = action.type.split("/")[1];
          // if (propName == "get_meetings_list") {
          //   // @ts-ignore
          //   console.log("entered2 = ", action.type, "payload = ", action.payload);
          //   // @ts-ignore
          //   state.calendarList = action.payload;
          // }
        }
      );
  },
});

export const dashboardSelector = (state: any) => state.dashboard;
export default dashboardSlice.reducer;
export const dashboardActions = dashboardSlice.actions;
