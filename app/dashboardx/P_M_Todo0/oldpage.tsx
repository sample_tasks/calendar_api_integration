"use client";
import React, { useEffect, useState, useCallback, useRef, useLayoutEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { format, parse, startOfWeek, getDay, endOfWeek, isSameDay, startOfMonth, endOfMonth, subDays, subWeeks, subMonths, addDays, addWeeks, addMonths } from "date-fns";
import { enUS } from "date-fns/locale";
import Link from "next/link";

import {
    Calendar,
    momentLocalizer,
    dateFnsLocalizer,
    // @ts-ignore
} from "react-big-calendar";

import "./style.css";

import SideMenu from "@/app/dashboard/component/SideMenu";
import "react-big-calendar/lib/css/react-big-calendar.css"; // Import the calendar styles
import { getMeetingsList, getMeetingInfo, dashboardSelector, dashboardActions } from "@/store/reducers/dashboard";
import Dialog from '@mui/material/Dialog';
import {
    Button, Badge, Menu, MenuItem, Backdrop, Tabs, Tab,
    Grid
} from "@mui/material";
import Eye from ".././../../public/image/eye.png";
import Download from ".././../../public/image/download.png";
import Gmeet from ".././../../public/image/gmeet.png"
import Image from 'next/image';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const locales = {
    "en-US": enUS,
};

const localizer = dateFnsLocalizer({
    format,
    parse,
    startOfWeek,
    getDay,
    locales,
});

const CustomDateCellWrapper = (props: any) => {
    const { children, value, ...rest } = props;
    const cellRef = useRef(null);

    useLayoutEffect(() => {
        const cell = cellRef.current;
        if (cell) {
            // @ts-ignore
            const eventsInCell = cell.querySelectorAll('.rbc-event');
            // console.log("length = ", cell);
            if (eventsInCell.length > 1) {
                const overlay = document.createElement('div');
                overlay.className = 'event-overlay';
                overlay.innerText = `+${eventsInCell.length - 1} more`;
                // @ts-ignore
                cell.appendChild(overlay);
            }
        }
    }, [cellRef]);

    return (
        <div ref={cellRef} {...rest} name="testDiv">
            {children}
        </div>
    );
};

const CustomEventContainer = (props: any) => {
    const { children, className, style } = props;
    // Access the props object
    const childrenProps = children?.props;

    // Get the length of the children array
    const childrenLength = Array.isArray(childrenProps?.children) ? childrenProps?.children.length : 0;

    console.log("CustomEventContainer = ", props, "lenth = ", childrenLength, "children?.props = ", children?.props, "childrenProps?.children = ", childrenProps?.children);
    return (
        <>
            {children}
        </>
    );
};

export default function P_M_Todo0() {

    const dispatch = useDispatch();
    // const sample = useSelector(dashboardSelector);
    const dashboardData = useSelector(dashboardSelector)?.get_meetings_list;
    const eventData = useSelector(dashboardSelector)?.get_meetings_info || {};

    const myEventsList = [
        {
            title: "Event 1",
            start: new Date(),
            end: new Date(new Date().setHours(new Date().getHours() + 1)),
        },
        {
            title: "Event 2",
            start: new Date(),
            end: new Date(new Date().setHours(new Date().getHours() + 1)),
        },
        {
            title: "Event 3",
            start: new Date(),
            end: new Date(new Date().setHours(new Date().getHours() + 1)),
        },
    ];

    // const data = [
    //   {
    //     "id": 3,
    //     "summary": "First level cloud interview",
    //     "desc": "First Level Interview",
    //     "start": "2024-06-09T23:00:00+05:30",
    //     "end": "2024-06-09T23:30:00+05:30",
    //     "attendees": "[\"test_1@gmail.com\", \"test_2@gmail.com\"]",
    //     "status": null,
    //     "comment": null,
    //     "score": {
    //       "django": 10
    //     },
    //     "link": "https://meet.google.com/tac-gkjx-yhn",
    //     "user_det": {
    //       "id": 2,
    //       "question_score": null,
    //       "status": null,
    //       "candidate": {
    //         "id": 9,
    //         "candidate_firstName": "yogeshwari",
    //         "candidate_lastName": "yogi",
    //         "candidateGender": "female",
    //         "candidateComment": "",
    //         "candidate_email": "yogeshwari@dataterrain.com",
    //         "candidate_dob": "2001-06-06T18:24:47+05:30",
    //         "candidate_phoneNumber": "+12125552368",
    //         "candidateAppliedDate": "2024-06-06T18:24:50+05:30",
    //         "candidate_isActive": true,
    //         "candidate_keySkill_Experience": {
    //           "html": 4,
    //           "python": 4
    //         },
    //         "candidate_yoe": null,
    //         "candidateOtherSkills": {
    //           "html": 4,
    //           "python": 4
    //         },
    //         "candidateQualification": "B.E",
    //         "candidateWorkMode": "onsite",
    //         "candidate_noticePeriod": "30",
    //         "candidate_location": "madurai",
    //         "candidateDistance": "400",
    //         "candidateTotalExperience": null,
    //         "candidate_onNotice": null,
    //         "candidate_currentSalary": 0,
    //         "candidate_expectedSalary": 0,
    //         "candidate_resume": "/media/uploads/Boopathy_S_Resume_FLZuhkl.pdf",
    //         "candidate_IsWillingToMove": false,
    //         "candidate_resume_modification": null,
    //         "candidateProfileImage": "/media/static/images/candidate_avatar_default.svg"
    //       },
    //       "handled_by": {
    //         "id": 3,
    //         "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
    //         "last_login": null,
    //         "userEmail": "vinodhini_hr@dataterrain.com",
    //         "username": "vinodhinihr",
    //         "firstName": "Vinodhini HR",
    //         "lastName": "vino",
    //         "userRole": "hr_employee",
    //         "userPhoneNumber": "",
    //         "userActiveDate": "2024-06-06T12:53:50.280843+05:30",
    //         "userinActiveDate": null,
    //         "userModifiedDate": "2024-06-14T11:19:33.439586+05:30",
    //         "userdateofbirth": "2024-06-11T00:00:00+05:30",
    //         "userbloodgroup": "o+ve",
    //         "userdescription": null,
    //         "usercity": "Madurai",
    //         "userstate": "tamil nadu",
    //         "userzipcode": "600089",
    //         "usercountry": "india",
    //         "userProfileImage": "/media/static/profile_images/jd_avatar_default_jrmUMBY.svg",
    //         "is_staff": false,
    //         "is_active": true,
    //         "is_superuser": false,
    //         "groups": [],
    //         "user_permissions": []
    //       },
    //       "job_id": {
    //         "id": 16,
    //         "jobRequest_Title": "UI Designer",
    //         "jobRequest_Role": "Senior Software Engineer",
    //         "jobRequest_KeySkills": "figma, html, css , js, adobe",
    //         "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
    //         "jobRequest_Expected_YOE": "3 to 5",
    //         "jobRequest_TotalVacancy": 1,
    //         "jobRequest_FilledVacancy": 0,
    //         "jobRequest_isActive": true,
    //         "jobRequest_isClosed": false,
    //         "jobRequest_has_Started": true,
    //         "jobRequest_isPriority": false,
    //         "jobRequest_PriorityValue": 0,
    //         "jobRequest_MoveToAssets": false,
    //         "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
    //         "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
    //         "jobRequest_createdBy": 4
    //       }
    //     },
    //     "job_id": {
    //       "id": 16,
    //       "jobRequest_Title": "UI Designer",
    //       "jobRequest_Role": "Senior Software Engineer",
    //       "jobRequest_KeySkills": "figma, html, css , js, adobe",
    //       "jobRequest_Description": "Experienced Front end developer with good skills in adobe",
    //       "jobRequest_Expected_YOE": "3 to 5",
    //       "jobRequest_TotalVacancy": 1,
    //       "jobRequest_FilledVacancy": 0,
    //       "jobRequest_isActive": true,
    //       "jobRequest_isClosed": false,
    //       "jobRequest_has_Started": true,
    //       "jobRequest_isPriority": false,
    //       "jobRequest_PriorityValue": 0,
    //       "jobRequest_MoveToAssets": false,
    //       "jobRequest_createdOn": "2024-06-06T17:54:02.939949+05:30",
    //       "jobRequest_Icon": "/media/static/images/jd_avatar_default.svg",
    //       "jobRequest_createdBy": {
    //         "id": 4,
    //         "password": "pbkdf2_sha256$600000$rawrshlQHIZHae13pTGfTt$qKtfw++/01tONB0U8DRFXbUInup0aGM4H6HkqztOBtQ=",
    //         "last_login": null,
    //         "userEmail": "dilli_project_manager@dataterrain.com",
    //         "username": "dillipm",
    //         "firstName": "Dilli HR",
    //         "lastName": "Rathnam",
    //         "userRole": "project_manager",
    //         "userPhoneNumber": "+12125552368",
    //         "userActiveDate": "2024-06-06T12:57:57.784744+05:30",
    //         "userinActiveDate": null,
    //         "userModifiedDate": "2024-06-12T19:57:06.631331+05:30",
    //         "userdateofbirth": null,
    //         "userbloodgroup": null,
    //         "userdescription": null,
    //         "usercity": "chennai",
    //         "userstate": null,
    //         "userzipcode": "600099",
    //         "usercountry": null,
    //         "userProfileImage": "/media/static/images/employee_avatar_default.svg",
    //         "is_staff": false,
    //         "is_active": true,
    //         "is_superuser": false,
    //         "groups": [],
    //         "user_permissions": []
    //       }
    //     }
    //   }
    // ];

    // const eventMap = (dashboardData || []).map((ele: any) => {
    //   return {
    //     id: ele?.id,
    //     title: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title,
    //     start: new Date(ele?.start),
    //     end: new Date(ele?.end),
    //     role: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title || "",
    //     interviewer: ele?.user_det && ele?.user_det?.handled_by && `${(ele?.user_det?.handled_by?.firstName || "") + (ele?.user_det?.handled_by?.lastName || "")}` || "",
    //     time: ele?.start,
    //     detailView: {
    //       interviewWith: ele?.user_det && ele?.user_det?.candidate && `${(ele?.user_det?.candidate?.candidate_firstName || "") + (ele?.user_det?.candidate?.candidate_lastName || "")}` || "",
    //       position: ele?.job_id && ele?.job_id?.jobRequest_Title || "",
    //       created_by: ele?.job_id?.jobRequest_createdBy && ele?.job_id?.jobRequest_createdBy?.userRole || "",
    //       interview_date: ele?.start,
    //       interview_time: ele?.end,
    //       interview_via: "Google Meet"
    //     }
    //   }
    // })

    const [selectedMonth, setSelectedMonth] = useState("");
    const [selectedYear, setSelectedYear] = useState("");
    const [activeEventModal, setActiveEventModal] = useState();
    const [anchorEl, setAnchorEl] = useState(null);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [events, setEvents] = useState([]);
    const [view, setView] = useState("week");
    const [date, setDate] = useState(new Date());

    const calendarView = [
        { label: 'Today', value: "today" },
        { label: 'Month', value: 'month' },
        { label: 'Week', value: 'week' },
        { label: 'Day', value: 'day' }
    ]

    const onPrevClick = useCallback(() => {
        if (view === "day") {
            // @ts-ignore
            setDate(subDays(date, 1));
        } else if (view === "week") {
            // @ts-ignore
            setDate(subWeeks(date, 1));
        } else {
            // @ts-ignore
            setDate(subMonths(date, 1));
        }
    }, [view, date]);

    const onNextClick = useCallback(() => {
        if (view === "day") {
            // @ts-ignore
            setDate(addDays(date, 1));
        } else if (view === "week") {
            // @ts-ignore
            setDate(addWeeks(date, 1));
        } else {
            // @ts-ignore
            setDate(addMonths(date, 1));
        }
    }, [view, date]);

    useEffect(() => {
        if (dashboardData) {
            const eventMap = (dashboardData || []).map((ele: any) => {
                return {
                    id: ele?.id,
                    title: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title,
                    start: new Date(ele?.start),
                    end: new Date(ele?.end),
                    role: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title || "",
                    interviewer: ele?.user_det && ele?.user_det?.handled_by && `${(ele?.user_det?.handled_by?.firstName || "") + (ele?.user_det?.handled_by?.lastName || "")}` || "",
                    time: ele?.start,
                    detailView: {
                        interviewWith: ele?.user_det && ele?.user_det?.candidate && `${(ele?.user_det?.candidate?.candidate_firstName || "") + (ele?.user_det?.candidate?.candidate_lastName || "")}` || "",
                        position: ele?.job_id && ele?.job_id?.jobRequest_Title || "",
                        created_by: ele?.job_id?.jobRequest_createdBy && ele?.job_id?.jobRequest_createdBy?.userRole || "",
                        interview_date: ele?.start,
                        interview_time: ele?.end,
                        interview_via: "Google Meet"
                    }
                }
            })

            // Helper function to parse date strings into Date objects
            function parseDate(dateString: any) {
                return new Date(dateString);
            }

            // Helper function to format the date and hour into a string for grouping
            function getDateHourString(date: any) {
                return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}`;
            }
            // @ts-ignore
            // const groupedEvents: any[] = [];
            // const processed = new Set();

            // events.forEach((event, index) => {
            //   // @ts-ignore
            //   if (processed.has(event.id)) {
            //     return; // Skip already grouped events
            //   }

            //   const group = [event];
            //   // @ts-ignore
            //   processed.add(event.id);
            //   // @ts-ignore
            //   const eventStart = parseDate(event.start);
            //   const eventDateHourString = getDateHourString(eventStart);

            //   for (let i = index + 1; i < events.length; i++) {
            //     const nextEvent = events[i];
            //     // @ts-ignore
            //     const nextEventStart = parseDate(nextEvent.start);
            //     const nextEventDateHourString = getDateHourString(nextEventStart);

            //     if (eventDateHourString === nextEventDateHourString) {
            //       group.push(nextEvent);
            //       // @ts-ignore
            //       processed.add(nextEvent.id);
            //     }
            //   }

            //   if (group.length > 1) {
            //     // @ts-ignore
            //     event.grouped = true;
            //     // @ts-ignore
            //     event.groupItems = group.slice(1);
            //   }

            //   groupedEvents.push(event);
            // });

            // Group events by their date and hour
            const groupedEventsMap = (eventMap || []).reduce((acc, event) => {
                // @ts-ignore
                const eventStart = parseDate(event.start);
                const key = getDateHourString(eventStart);
                // @ts-ignore
                if (!acc[key]) {
                    // @ts-ignore
                    acc[key] = [];
                }
                // @ts-ignore
                acc[key].push(event);
                return acc;
            }, {});
            // @ts-ignore
            const groupedEvents = [];
            // Object.values(groupedEventsMap).forEach(group => {
            //   // @ts-ignore
            //   if (group.length > 1) {
            //     // @ts-ignore
            //     const groupItems = group.map(event => ({
            //       ...event,
            //       groupItems: group,
            //       grouped: true,
            //       firstGroup: false
            //     }));

            //     // Mark the first event in the group as the firstGroup
            //     groupItems[0].firstGroup = true;
            //     groupedEvents.push(...groupItems);
            //   } else {
            //     // @ts-ignore
            //     groupedEvents.push(...group);
            //   }
            // });

            // ------------------------------------------

            Object.values(groupedEventsMap).forEach(group => {
                // @ts-ignore
                if (group.length > 1) {
                    // Mark the first event in the group with grouped and firstGroup properties
                    // @ts-ignore
                    const firstEvent = group[0];
                    firstEvent.grouped = true;
                    firstEvent.firstGroup = true;
                    firstEvent.groupItems = group;
                    groupedEvents.push(firstEvent);
                } else {
                    // @ts-ignore
                    groupedEvents.push(...group);
                }
            });

            // console.log(JSON.stringify(groupedEvents, null, 2));
            // @ts-ignore
            console.log("groupedEvents = ", groupedEvents);
            // @ts-ignore
            setEvents(groupedEvents);
        }
    }, [dashboardData]);

    useEffect(() => {
        const handleCalendar = () => {
            const today = new Date();
            // @ts-ignore
            let monthStartDate = startOfWeek(today);
            // @ts-ignore
            let monthEndDate = endOfWeek(today);
            let queryString = `from_date=${format(monthStartDate, 'yyyy-MM-dd')}&to_date=${format(monthEndDate, 'yyyy-MM-dd')}`;
            // @ts-ignore
            dispatch(getMeetingsList({ data: queryString }));
        }
        handleCalendar();
    }, [])

    const cellRefs = useRef([]);

    useEffect(() => {
        const cells = cellRefs.current;
        // console.log("sfd = ",cells);
        cells.forEach((cell: any) => {
            const eventsInCell = cell.querySelectorAll('.rbc-event');
            if (eventsInCell.length > 1) {
                const overlay = document.createElement('div');
                overlay.className = 'event-overlay';
                overlay.innerText = `+${eventsInCell.length - 1} more`;
                cell.appendChild(overlay);
            }
        });
    }, []);

    //   useEffect(() => {
    //     const handleCalendar = () => {
    //       const today = new Date();
    //       // @ts-ignore
    //       let monthStartDate = view == "week" ? startOfWeek(today): view == "month" ? startOfMonth(today) : startOfWeek(today);
    //       // @ts-ignore
    //       let monthEndDate = view == "week" ? endOfWeek(today): view == "month" ? endOfMonth(today) : endOfWeek(today);
    //       let queryString = `from_date=${format(monthStartDate, 'yyyy-MM-dd')}&to_date=${format(monthEndDate, 'yyyy-MM-dd')}`;
    //       // @ts-ignore
    //       dispatch(getMeetingsList({ data: queryString }));
    //     }
    // handleCalendar();  
    //   },[view, ])

    // Define months and years
    const months = [
        { value: "01", label: "January" },
        { value: "02", label: "February" },
        { value: "03", label: "March" },
        { value: "04", label: "April" },
        { value: "05", label: "May" },
        { value: "06", label: "June" },
        { value: "07", label: "July" },
        { value: "08", label: "August" },
        { value: "09", label: "September" },
        { value: "10", label: "October" },
        { value: "11", label: "November" },
        { value: "12", label: "December" },
    ];

    const years = [
        { value: "2022", label: "2022" },
        { value: "2023", label: "2023" },
        { value: "2024", label: "2024" },
        // Add more years as needed
    ];

    // Handle month and year changes
    const handleMonthChange = (e: {
        target: { value: React.SetStateAction<string> };
    }) => {
        setSelectedMonth(e.target.value);
        // @ts-ignore
        const month = parseInt(e.target.value, 10); // Subtract 1 because months are zero-indexed in JavaScript
        const year = parseInt(selectedYear, 10);
        setDate(new Date(year || 2024, month || 0, 1));
    };

    const handleYearChange = (e: {
        target: { value: React.SetStateAction<string> };
    }) => {
        setSelectedYear(e.target.value);
        const month = parseInt(selectedMonth, 10); // Subtract 1 because months are zero-indexed in JavaScript
        // @ts-ignore
        const year = parseInt(e.target.value, 10);
        console.log("year = ", year, "month = ", month)
        setDate(new Date(year || 2024, month || 0, 1));
    };


    const handleSelectSlot = (event: any) => {
        if (typeof event.start === "string") {
            event.start = new Date(event.start);
        }

        if (typeof event.end === "string") {
            event.end = new Date(event.end);
        }

        setActiveEventModal(event);

    };

    const handleSelect = (event: any, e: any) => {
        // console.log("handleselect = ", event);
        if (event?.grouped) {
            setAnchorEl(e.currentTarget);
            setActiveEventModal(event);
        } else {
            //  @ts-ignore
            dispatch(getMeetingInfo({ data: `id=${event.id}` }));
        }
        const { start, end } = event;
        // setActiveEventModal(event);
        setPosition({ x: e.clientX, y: e.clientY });
    };

    const getOrdinalSuffix = (day: any) => {
        if (day > 3 && day < 21) return 'th'; // covers 11th, 12th, 13th
        switch (day % 10) {
            case 1: return 'st';
            case 2: return 'nd';
            case 3: return 'rd';
            default: return 'th';
        }
    };

    const formatDateWithOrdinal = (date: any) => {
        const day = format(date, 'd');
        const dayWithSuffix = `${day}${getOrdinalSuffix(parseInt(day, 10))}`;
        const monthAndYear = format(date, 'MMM yyyy');
        return `${dayWithSuffix} ${monthAndYear}`;
    };
    //  @ts-ignore
    const handleClose = (event, e) => {
        e.stopPropagation();
        setAnchorEl(null);
        //  @ts-ignore
        setActiveEventModal(false);
        //  @ts-ignore
        dispatch(getMeetingInfo({ data: `id=${event.id}` }));
    }

    const handleCloseOnly = (e: any) => {
        e.stopPropagation();
        setAnchorEl(null);
        //  @ts-ignore
        setActiveEventModal(false);
    }

    const EventCell = (props: any) => {
        const { event, style } = props;
        console.log("eventcell = ", event);
        return <>
            <div className={`calendarTopSection`} style={style}>
                <ul>
                    <li className="text-[12px] py-1">{event?.title}</li>
                    <li className="text-[12px] py-1">{event?.interviewer}</li>
                    <li className="text-[12px] py-1">Time : {`${formatDate(event?.start)} - ${formatDate(event?.end)}`}</li>
                    {/* <li className="text-[12px] py-1">Via : Google Voice</li> */}
                </ul>
            </div>
        </>
    }

    const EventListModel = () => {
        //  @ts-ignore
        const data = activeEventModal?.grouped || {};
        console.log("entered = ")
        return (
            //  @ts-ignore
            activeEventModal && activeEventModal?.grouped ? (
                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme: any) => theme.zIndex.drawer + 1 }}
                    open={activeEventModal}
                    //  @ts-ignore
                    onClick={handleCloseOnly}
                >
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={activeEventModal}
                        onClose={() => setAnchorEl(null)}
                        slotProps={
                            {
                                paper: {
                                    elevation: 0,
                                    sx: {
                                        overflow: 'visible',
                                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                                        mt: 1.5,
                                        maxWidth: '460px',
                                        maxHeight: "100%",
                                        backgroundColor: 'transparent',
                                        top: "0 !important"
                                        // '& .MuiAvatar-root': {
                                        //     width: 32,
                                        //     height: 32,
                                        //     ml: -0.5,
                                        //     mr: 1,
                                        // },
                                        // left: sm ? '8px !important' : 'auto',
                                        // overflow: sm ? 'auto' : 'visible',
                                        // borderRadius: '10px',
                                        // '&::before': {
                                        //     content: '""',
                                        //     display: 'block',
                                        //     position: 'absolute',
                                        //     top: sm ? 0 : 15,
                                        //     left: sm ? 0 : -5,
                                        //     // right: 14,
                                        //     width: sm ? 0 : 10,
                                        //     height: sm ? 0 : 10,
                                        //     bgcolor: 'background.paper',
                                        //     transform: 'translateY(-50%) rotate(45deg)',
                                        //     zIndex: 0,
                                        // },
                                    },
                                }
                            }
                        }
                    >
                        {/* @ts-ignore */}
                        {(activeEventModal?.groupItems || []).map((ele, i) => <MenuItem
                            key={`${ele?.id}-${i}`}
                            // @ts-ignore
                            onClick={(e) => handleClose(ele, e)}
                        >
                            <EventCell event={ele} style={{ position: 'unset' }} />
                        </MenuItem>)}
                    </Menu>
                </Backdrop>
            ) : null
        );
    };

    const EventDetailModal = () => {
        //  @ts-ignore
        const data = eventData?.detailView || {};
        return (
            //  @ts-ignore
            eventData && eventData.detailView ? (
                // <div
                //   style={{
                //     position: "absolute",
                //     top: 0,
                //     left: 0,
                //     width: "100%",
                //     backgroundColor: "white",
                //     border: "1px solid black",
                //     padding: "10px",
                //     color: "blue",
                //     height: "100%",
                //     zIndex: 1000,
                //   }}
                // >
                <Dialog
                    classes={{
                        paper: "paper"
                    }}
                    onClose={(e) => {
                        //  @ts-ignore
                        e.stopPropagation();
                        //  @ts-ignore
                        // setActiveEventModal(false);
                        dispatch(dashboardActions.clearData());
                    }}
                    open={eventData}
                >
                    <div className="modelContainer">
                        <div className="innerContainer">
                            <div className="innerItem">
                                <span>{`Interview With: ${data.interviewWith}`}</span>
                                <span>{`Position: ${data.position}`}</span>
                                <span>{`Created By: ${data.created_by}`}</span>
                                <span>{`Interview Date: ${formatDateWithOrdinal(data.interview_date)}`}</span>
                                <span>{`Interview Time:  ${formatDate(eventData?.start)} - ${formatDate(eventData?.end)}`}</span>
                                <span>{`Interview Via: Google Meet`}</span>
                                <Button className="btn1">
                                    Resume Docs
                                    <Image src={Eye} alt="eye img" className="eyeImg" />
                                    <Image src={Download} alt="download img" className="downloadImg" />
                                </Button>
                                <Button className="btn1">
                                    Adhaar Card
                                    <Image src={Eye} alt="eye img" className="eyeImg" />
                                    <Image src={Download} alt="download img" className="downloadImg" />
                                </Button>
                            </div>
                            <div className="innerItem">
                                <Image src={Gmeet} alt="gmeet image" className="gmeetImg" />
                                <Button className="btn2">
                                    Join
                                </Button>
                            </div>
                        </div>
                    </div>
                </Dialog>
                // </div>
            ) : null
        );
    };

    // {activeEventModal?.title && (
    //   <div
    //     style={{
    //       position: "absolute",
    //       top: 0,
    //       left: 0,
    //       width: "100%",
    //       backgroundColor: "white",
    //       border: "1px solid black",
    //       padding: "10px",
    //       color: "blue",
    //       height: "100%",
    //       zIndex: 1000,
    //     }}
    //   >
    //     {/* @ts-ignore */}
    //     {activeEventModal?.title}
    //   </div>
    // )}

    const formatDate = (inoutdate: any) => {
        const date = new Date(inoutdate);

        // Format the time part
        const time = format(date, 'hh aa');

        return time
    }

    // const formatTime = (indate: any, outdate: any) => {
    //   let date1 = "";
    //   let date2 = ""
    //   if (indate) {
    //     date1 = formatDate(indate);
    //   }
    //   else if(outdate){
    //     date2 = formatDate(outdate);
    //   }
    //   if(date1.includes("PM"))
    // }

    // Custom Event Component
    const CustomEvent = (props: any) => {
        const { event } = props;
        // @ts-ignore
        console.log("customeevent = ", props, "activeEventModal = ", activeEventModal, "cond = ", activeEventModal && activeEventModal?.id == event?.id && event?.grouped);
        // const startDate = format(event?.start, 'MM/dd/yyyy');
        // const totalEvent = document.getElementsByClassName(startDate);
        // console.log("totalEvent = ",totalEvent);
        return (<>
            {event?.grouped && event?.firstGroup && view == "week" ? <>
                {/* @ts-ignore */}
                <div className={`calendarTopSection`} style={{ position: 'absolute' }}>
                    <ul>
                        <li className="text-[12px] py-1">{event?.title}</li>
                        <li className="text-[12px] py-1">{event?.interviewer}</li>
                        <li className="text-[12px] py-1">Time : {`${formatDate(event?.start)} - ${formatDate(event?.end)}`}</li>
                        {/* <li className="text-[12px] py-1">Via : Google Voice</li> */}
                    </ul>
                </div>
                <div className="badge"><span>{event?.groupItems.length}</span></div>
            </> : (!event?.grouped || view != "week") ?
                <EventCell event={event} style={{ position: 'absolute' }} /> : null}
            {/* @ts-ignore */}
            {(activeEventModal && activeEventModal?.id == event?.id && event?.grouped) ? <EventListModel /> : eventData?.id == event?.id ? <EventDetailModal /> : null}
        </>
        );
    };
    // const MyEventWrapper = (props: any) => {
    //   console.log("props5rg", props);
    // }
    //  @ts-ignore
    // const CustomDayWrapper = (props) => {
    //   const { children, value, range, events } = props;
    //   console.log("events = ", props);
    //   return <h3>test</h3>
    //   // @ts-ignore
    //   // const eventsInRange = events.filter(event =>
    //   //   //  @ts-ignore
    //   //   isSameDay(event.start, value)
    //   // );

    //   // if (eventsInRange.length > 1) {
    //   //   return (
    //   //     <div className="custom-day-wrapper">
    //   //       <span className="event-badge">{eventsInRange.length} events</span>
    //   //     </div>
    //   //   );
    // }

    //   return children;
    // };

    // const getEventCount = (date) => {
    //   //  @ts-ignore
    //   console.log("data = ", data);
    //   const eventCount = events.filter((event: any) =>
    //     event.start <= date && event.end >= date
    //   ).length;
    //   return eventCount > 0 ? { className: 'custom-cell', style: { backgroundColor: '#ffc107', color: 'black' } } : {};
    // };

    console.log("eventlist = ", dashboardData, "eventMap = ", events);

    const onRangeChange = useCallback((range: any) => {
        // console.log(range);
        const handleCalendar = () => {
            const today = new Date();
            // @ts-ignore
            let monthStartDate = Array.isArray(range) ? range[0] : range?.start;
            // @ts-ignore
            let monthEndDate = Array.isArray(range) ? range[range.length - 1] : range?.end;
            let queryString = `from_date=${format(monthStartDate, 'yyyy-MM-dd')}&to_date=${format(monthEndDate, 'yyyy-MM-dd')}`;
            // @ts-ignore
            dispatch(getMeetingsList({ data: queryString }));
        }
        handleCalendar();
    }, [])

    // const eventPropGetter = (props: any) => {
    //   console.log("eventPropGetter = ", props);
    // }

    // const CustomDayCell = (props:any) => {
    //   const { children } = props;
    //   console.log("customdaycel = ",props);
    //   if (!children || children.length === 0) return null;

    //   const firstEvent = children[0];
    //   const totalEvents = children.length;

    //   return (
    //     <div className="custom-day-cell">
    //       <CustomEvent event={firstEvent} title={firstEvent.title} totalEvents={totalEvents} />
    //     </div>
    //   );
    // };
    // const onView = useCallback((newView:any) => setView(newView), [setView])

    const CustomHeader = (props: any) => {
        const { label, date } = props;
        console.log("CustomHeader = ", props);
        // const date = parse(label, "MMM dd", new Date());
        return (
            <div>
                <div>{format(date, "dd MMM")}</div>
                <div>{format(date, "EEEE")}</div>
            </div>
        );
    };

    return (
        <section className="">
            <div className="container-fluid my-md-5 my-4">
                <div className="row">
                    <div className="col-lg-1 leftMenuWidth ps-0 position-relative">
                        <SideMenu />
                    </div>

                    <div className="col-lg-11 pe-lg-4 ps-lg-0">
                        <div className="row justify-content-between align-items-center">
                            <div className="col-lg-8 projectText">
                                <h1>Calendar</h1>
                                <p className="mt-3">
                                    Enjoy your selecting potential candidates Tracking and
                                    Management System.
                                </p>
                            </div>

                            <div className="col-lg-4 mt-3 mt-lg-0 text-center text-lg-end">
                                <Link
                                    prefetch
                                    href="/P_M_JobDescriptions1"
                                    className="btn btn-light me-3 mx-lg-2"
                                >
                                    JD Assets
                                </Link>
                                <Link
                                    prefetch
                                    href="P_M_JobDescriptions4"
                                    className="btn btn-blue bg-[#0a66c2!important]"
                                >
                                    Create New JD
                                </Link>
                            </div>
                        </div>

                        <div className="TotalEmployees shadow bg-white rounded-3 p-3 w-100 mt-4">
                            <div className="md:flex align-items-center">
                                <h3 className="projectManHeading">Your Todo’s</h3>
                                <div className="ml-auto d-flex todoHeadingSelect">
                                    <div className="month-year-picker">
                                        <select value={selectedMonth} onChange={handleMonthChange}>
                                            {/* <option value="">Select Month</option> */}
                                            {months.map((month) => (
                                                <option key={month.value} value={month.value}>
                                                    {month.label}
                                                </option>
                                            ))}
                                        </select>
                                        <select value={selectedYear} onChange={handleYearChange}>
                                            {/* <option value="">Select Year</option> */}
                                            {years.map((year) => (
                                                <option key={year.value} value={year.value}>
                                                    {year.label}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div
                                className="d-none d-lg-block "
                                style={{ width: "100%", position: "relative" }}
                            >
                                <div className="toolbar">
                                    <div>
                                        <Button
                                            onClick={onPrevClick}
                                            className="navigatebtn"
                                        >
                                            <ArrowBackIosNewIcon />
                                        </Button>
                                        <Button
                                            onClick={onNextClick}
                                            className="navigatebtn"
                                        >
                                            <ArrowForwardIosIcon sx={{ strokeWidth: 1 }} />
                                        </Button>
                                    </div>
                                    <div>
                                        <Tabs
                                            value={view}
                                            // @ts-ignore
                                            onChange={(e, newView) => {
                                                // @ts-ignore
                                                if (newView == "today") {
                                                    setDate(new Date())
                                                }
                                                else if (newView) setView(newView);
                                            }}
                                            className={"tabs"}
                                        >
                                            {calendarView.map((tb, i) => (
                                                <Tab value={tb.value} label={tb.label} key={`${i}_${tb.value}`} />)
                                            )}
                                        </Tabs>
                                    </div>
                                </div>
                                <Calendar
                                    className="TodoDataTable"
                                    dayLayoutAlgorithm={'no-overlap'}
                                    selectable
                                    popup={true}
                                    localizer={localizer}
                                    events={events}
                                    startAccessor="start"
                                    endAccessor="end"
                                    style={{ height: 600 }}
                                    date={date}
                                    defaultView={"week"}
                                    view={view}
                                    timeslots={4} // number of per section
                                    step={15}
                                    views={{ month: true, week: true, day: true }} // Show only month, week, and day views
                                    components={{
                                        event: CustomEvent,
                                        week: {
                                            header: CustomHeader,
                                        },
                                        // eventWrapper: MyEventWrapper,
                                        // dateCellWrapper: CustomDayWrapper,
                                        // month: {
                                        //   event: CustomDayCell
                                        // },
                                        // day: {
                                        //   event: CustomDayCell
                                        // },
                                        // week: {
                                        //   event: CustomDayCell
                                        // }
                                        // @ts-ignore
                                        //   dateCellWrapper: ({ children, value, ...props }) => (
                                        //     <div
                                        //       ref={(el:any) => {
                                        //         // @ts-ignore
                                        //         if (el && !cellRefs.current.includes(el)) {
                                        //           // @ts-ignore
                                        //           cellRefs.current.push(el);
                                        //         }
                                        //       }}
                                        //       {...props}
                                        //     >
                                        //       {children}
                                        //     </div>
                                        //   ),
                                        // dateCellWrapper: CustomDateCellWrapper,
                                        // eventContainerWrapper: CustomEventContainer,
                                    }}
                                    // onNavigate={(date:any, view:any) => {
                                    //   console.log('#### onNavigate');
                                    //   console.log('#### date=', date);
                                    //   console.log('#### view=', view);
                                    //   //this.setState({currentDate: date});
                                    // }}
                                    formats={{
                                        dayFormat: "EEEE", // day labels
                                    }}
                                    toolbar={false}
                                    // dayPropGetter={getEventCount}
                                    // eventPropGetter={eventPropGetter}
                                    onSelectSlot={handleSelectSlot}
                                    onSelectEvent={handleSelect}
                                    onRangeChange={onRangeChange}
                                // onView={onView}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

// const CustomEvent = (event:any) => {
//   console.log(event,"sadfsdfsd")
//   return (
//     <span> <strong> {event.title} </strong> </span>
//   )
// }
// Custom Toolbar Component
// const CustomToolbar = ({ label }: any) => {
//   return (
//     <div className="custom-toolbar ">
//       <strong>{label}</strong>
//       {/* Add custom buttons or components here */}
//     </div>
//   );
// };
