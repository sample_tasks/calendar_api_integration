"use client";
import React, { useEffect, useState, useCallback, useRef, useLayoutEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { format, parse, startOfWeek, getDay, endOfWeek, isSameDay, startOfMonth, endOfMonth, subDays, subWeeks, subMonths, addDays, addWeeks, addMonths, getMonth, getYear } from "date-fns";
import { enUS } from "date-fns/locale";
import Link from "next/link";

import {
  Calendar,
  dateFnsLocalizer,
  // @ts-ignore
} from "react-big-calendar";

import "./style.css";

import SideMenu from "@/app/dashboard/component/SideMenu";
import "react-big-calendar/lib/css/react-big-calendar.css"; // Import the calendar styles
import { getMeetingsList, getMeetingInfo, dashboardSelector, dashboardActions } from "@/store/reducers/dashboard";
import Dialog from '@mui/material/Dialog';
import {
  Button, Menu, MenuItem, Backdrop, Tabs, Tab,
} from "@mui/material";
import Eye from ".././../../public/image/eye.png";
import Download from ".././../../public/image/download-blue.png";
import Gmeet from ".././../../public/image/gmeet.png"
import Image from 'next/image';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const locales = {
  "en-US": enUS,
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

export default function P_M_Todo0() {

  const dispatch = useDispatch();

  const dashboardData = useSelector(dashboardSelector)?.get_meetings_list;
  const eventData = useSelector(dashboardSelector)?.get_meetings_info || {};

  const [selectedMonth, setSelectedMonth] = useState(format(new Date(), 'MM'));
  const [selectedYear, setSelectedYear] = useState(getYear(new Date()));
  const [activeEventModal, setActiveEventModal] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [events, setEvents] = useState([]);
  const [view, setView] = useState("week");
  const [date, setDate] = useState(new Date());

  const calendarView = [
    { label: 'Today', value: "today" },
    { label: 'Month', value: 'month' },
    { label: 'Week', value: 'week' },
    { label: 'Day', value: 'day' }
  ]

  const listApiCall = (currentDate: any, currentView: any) => {
    let startDate, endDate;

    switch (currentView) {
      case 'day':
        startDate = currentDate;
        endDate = currentDate;
        break;
      case 'week':
        startDate = startOfWeek(currentDate, { weekStartsOn: 0 }); // assuming week starts on Monday
        endDate = endOfWeek(currentDate, { weekStartsOn: 0 });
        break;
      case 'month':
        startDate = startOfMonth(currentDate);
        endDate = endOfMonth(currentDate);
        break;
      default:
        startDate = currentDate;
        endDate = currentDate;
    }


    let queryString = `from_date=${format(startDate, 'yyyy-MM-dd')}&to_date=${format(endDate, 'yyyy-MM-dd')}`;
    // @ts-ignore
    dispatch(getMeetingsList({ data: queryString }));

  };

  const onPrevClick = useCallback(() => {
    let current = new Date();
    if (view === "day") {
      // @ts-ignore
      current = subDays(date, 1);
      // setDate(subDays(date, 1));
    } else if (view === "week") {
      // @ts-ignore
      current = subWeeks(date, 1);
      // setDate(subWeeks(date, 1));
    } else {
      // @ts-ignore
      current = subMonths(date, 1);
      // setDate(subMonths(date, 1));
    }
    // @ts-ignore
    setDate(current);
    setSelectedYear(getYear(current));
    setSelectedMonth(format(current, "MM"));
    listApiCall(current, view);
  }, [view, date]);

  const onNextClick = useCallback(() => {
    let current = new Date();
    if (view === "day") {
      // @ts-ignore
      current = addDays(date, 1);
      // setDate(addDays(date, 1));
    } else if (view === "week") {
      // @ts-ignore
      current = addWeeks(date, 1);
      // setDate(addWeeks(date, 1));
    } else {
      // @ts-ignore
      current = addMonths(date, 1);
      // setDate(addMonths(date, 1));
    }
    setDate(current);
    setSelectedYear(getYear(current));
    setSelectedMonth(format(current, "MM"));
    listApiCall(current, view);
  }, [view, date]);

  useEffect(() => {
    if (dashboardData) {
      const eventMap = (dashboardData || []).map((ele: any) => {
        return {
          id: ele?.id,
          title: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title,
          start: new Date(ele?.start),
          end: new Date(ele?.end),
          role: ele?.user_det && ele?.user_det?.job_id && ele?.user_det?.job_id?.jobRequest_Title || "",
          interviewer: ele?.user_det && ele?.user_det?.handled_by && `${(ele?.user_det?.handled_by?.firstName || "") + (ele?.user_det?.handled_by?.lastName || "")}` || "",
          time: ele?.start,
          detailView: {
            interviewWith: ele?.user_det && ele?.user_det?.candidate && `${(ele?.user_det?.candidate?.candidate_firstName || "") + (ele?.user_det?.candidate?.candidate_lastName || "")}` || "",
            position: ele?.job_id && ele?.job_id?.jobRequest_Title || "",
            created_by: ele?.job_id?.jobRequest_createdBy && ele?.job_id?.jobRequest_createdBy?.userRole || "",
            interview_date: ele?.start,
            interview_time: ele?.end,
            interview_via: "Google Meet"
          }
        }
      })

      // Helper function to parse date strings into Date objects
      function parseDate(dateString: any) {
        return new Date(dateString);
      }

      // Helper function to format the date and hour into a string for grouping
      function getDateHourString(date: any) {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}`;
      }
      // @ts-ignore
      // Group events by their date and hour
      const groupedEventsMap = (eventMap || []).reduce((acc, event) => {
        // @ts-ignore
        const eventStart = parseDate(event.start);
        const key = view != "week" ? event.id : getDateHourString(eventStart);

        // @ts-ignore
        if (!acc[key]) {
          // @ts-ignore
          acc[key] = [];
        }
        // @ts-ignore
        acc[key].push(event);
        console.log("acc = ", acc);
        return acc;
      }, {});
      // @ts-ignore
      const groupedEvents = [];

      Object.values(groupedEventsMap).forEach(group => {
        console.log("group = ", group);
        // @ts-ignore
        if (group.length > 1) {
          // Mark the first event in the group with grouped and firstGroup properties
          // @ts-ignore
          const firstEvent = group[0];
          firstEvent.grouped = true;
          firstEvent.firstGroup = true;
          firstEvent.groupItems = group;
          groupedEvents.push(firstEvent);
        } else {
          // @ts-ignore
          groupedEvents.push(...group);
        }
      });

      // Object.values(groupedEventsMap).forEach(group => {
      //   // @ts-ignore
      //   if (group.length > 1) {
      //     // @ts-ignore
      //     const groupItems = group.map(event => ({
      //       ...event,
      //       groupItems: group,
      //       grouped: true,
      //       firstGroup: false
      //     }));

      //     // Mark the first event in the group as the firstGroup
      //     groupItems[0].firstGroup = true;
      //     groupedEvents.push(...groupItems);
      //   } else {
      //     // @ts-ignore
      //     groupedEvents.push(...group);
      //   }
      // });

      // @ts-ignore
      console.log("groupedEvents = ", groupedEvents);
      // @ts-ignore
      setEvents(groupedEvents);
    }
  }, [dashboardData, view]);

  useEffect(() => {
    const handleCalendar = () => {
      const today = new Date();
      // @ts-ignore
      let monthStartDate = startOfWeek(today);
      // @ts-ignore
      let monthEndDate = endOfWeek(today);
      let queryString = `from_date=${format(monthStartDate, 'yyyy-MM-dd')}&to_date=${format(monthEndDate, 'yyyy-MM-dd')}`;
      // @ts-ignore
      dispatch(getMeetingsList({ data: queryString }));
    }
    handleCalendar();
  }, [])

  // Define months and years
  const months = [
    { value: "01", label: "January" },
    { value: "02", label: "February" },
    { value: "03", label: "March" },
    { value: "04", label: "April" },
    { value: "05", label: "May" },
    { value: "06", label: "June" },
    { value: "07", label: "July" },
    { value: "08", label: "August" },
    { value: "09", label: "September" },
    { value: "10", label: "October" },
    { value: "11", label: "November" },
    { value: "12", label: "December" },
  ];

  const years = [
    { value: "2022", label: "2022" },
    { value: "2023", label: "2023" },
    { value: "2024", label: "2024" },
    // Add more years as needed
  ];

  // Handle month and year changes
  const handleMonthChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    // @ts-ignore
    setSelectedMonth(e.target.value);
    // @ts-ignore
    const month = parseInt(e.target.value, 10); // Subtract 1 because months are zero-indexed in JavaScript
    // @ts-ignore
    const year = parseInt(selectedYear, 10);
    let currentdate = new Date(year || 2024, month - 1 || 0, 1)
    setDate(currentdate);
    listApiCall(currentdate, view);
  };

  const handleYearChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    // @ts-ignore
    setSelectedYear(e.target.value);
    // @ts-ignore
    const month = parseInt(selectedMonth, 10); // Subtract 1 because months are zero-indexed in JavaScript
    // @ts-ignore
    const year = parseInt(e.target.value, 10);
    // console.log("year = ", year, "month = ", month)
    let currentdate = new Date(year || 2024, month - 1 || 0, 1)
    setDate(currentdate);
    listApiCall(currentdate, view);
  };

  const handleSelectSlot = (event: any) => {
    if (typeof event.start === "string") {
      event.start = new Date(event.start);
    }

    if (typeof event.end === "string") {
      event.end = new Date(event.end);
    }

    setActiveEventModal(event);

  };

  const handleSelect = (event: any, e: any) => {
    // console.log("handleselect = ", event);
    if (event?.grouped) {
      setAnchorEl(e.currentTarget);
      setActiveEventModal(event);
    } else {
      //  @ts-ignore
      dispatch(getMeetingInfo({ data: `id=${event.id}` }));
    }
    const { start, end } = event;
    // setActiveEventModal(event);
    setPosition({ x: e.clientX, y: e.clientY });
  };

  const getOrdinalSuffix = (day: any) => {
    if (day > 3 && day < 21) return 'th'; // covers 11th, 12th, 13th
    switch (day % 10) {
      case 1: return 'st';
      case 2: return 'nd';
      case 3: return 'rd';
      default: return 'th';
    }
  };

  const formatDateWithOrdinal = (date: any) => {
    const day = format(date, 'd');
    const dayWithSuffix = `${day}${getOrdinalSuffix(parseInt(day, 10))}`;
    const monthAndYear = format(date, 'MMM yyyy');
    return `${dayWithSuffix} ${monthAndYear}`;
  };
  //  @ts-ignore
  const handleClose = (event, e) => {
    e.stopPropagation();
    setAnchorEl(null);
    //  @ts-ignore
    setActiveEventModal(false);
    //  @ts-ignore
    dispatch(getMeetingInfo({ data: `id=${event.id}` }));
  }

  const handleCloseOnly = (e: any) => {
    e.stopPropagation();
    setAnchorEl(null);
    //  @ts-ignore
    setActiveEventModal(false);
  }

  const EventCell = (props: any) => {
    const { event, style } = props;
    // console.log("eventcell = ", event);
    return <>
      <div className={`calendarTopSection`} style={style}>
        <ul>
          <li className="text-[12px] py-1">{event?.title}</li>
          <li className="text-[12px] py-1">{event?.interviewer}</li>
          <li className="text-[12px] py-1">Time : {`${formatDate(event?.start)} - ${formatDate(event?.end)}`}</li>
          {/* <li className="text-[12px] py-1">Via : Google Voice</li> */}
        </ul>
      </div>
    </>
  }

  const EventListModel = () => {
    //  @ts-ignore
    // console.log("entered = ")
    return (
      //  @ts-ignore
      activeEventModal && activeEventModal?.grouped ? (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme: any) => theme.zIndex.drawer + 1, backgroundColor: "rgb(255 255 255 / 70%)" }}
          open={activeEventModal}
          //  @ts-ignore
          onClick={handleCloseOnly}
        >
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={activeEventModal}
            onClose={() => setAnchorEl(null)}
            slotProps={
              {
                paper: {
                  elevation: 0,
                  sx: {
                    overflow: 'visible',
                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    mt: 1.5,
                    maxWidth: '460px',
                    maxHeight: "100%",
                    backgroundColor: 'transparent',
                    top: "0 !important"
                  },
                }
              }
            }
          >
            {/* @ts-ignore */}
            {(activeEventModal?.groupItems || []).map((ele, i) => <MenuItem
              key={`${ele?.id}-${i}`}
              // @ts-ignore
              onClick={(e) => handleClose(ele, e)}
            >
              <EventCell event={ele} style={{ position: 'unset' }} />
            </MenuItem>)}
          </Menu>
        </Backdrop>
      ) : null
    );
  };

  const EventDetailModal = () => {
    //  @ts-ignore
    const data = eventData?.detailView || {};
    console.log("eventdetailview = ", data);
    return (
      //  @ts-ignore
      eventData && eventData.detailView ? (
        <Dialog
          classes={{
            paper: "paper"
          }}
          onClose={(e) => {
            //  @ts-ignore
            e.stopPropagation();
            //  @ts-ignore
            // setActiveEventModal(false);
            dispatch(dashboardActions.clearData());
          }}
          open={eventData}
          slotProps={{ backdrop: { sx: { background: 'transparent' } } }}
        >
          <div className="modelContainer">
            <div className="innerContainer">
              <div className="innerItem">
                <span>{`Interview With: ${data.interviewWith}`}</span>
                <span>{`Position: ${data.position}`}</span>
                <span>{`Created By: ${data.created_by}`}</span>
                <span>{`Interview Date: ${formatDateWithOrdinal(data.interview_date)}`}</span>
                <span>{`Interview Time:  ${formatDate(eventData?.start)} - ${formatDate(eventData?.end)}`}</span>
                <span>{`Interview Via: Google Meet`}</span>
                <Button className="btn1">
                  Resume Docs
                  <Image src={Eye} alt="eye img" className="eyeImg" />
                  <Image src={Download} alt="download img" className="downloadImg" />
                </Button>
                <Button className="btn1">
                  Adhaar Card
                  <Image src={Eye} alt="eye img" className="eyeImg" />
                  <Image src={Download} alt="download img" className="downloadImg" />
                </Button>
              </div>
              <div className="innerItem">
                <Image src={Gmeet} alt="gmeet image" className="gmeetImg" />
                <Button className="btn2">
                  Join
                </Button>
              </div>
            </div>
          </div>
        </Dialog>
      ) : null
    );
  };

  const formatDate = (inoutdate: any) => {
    const date = new Date(inoutdate);

    // Format the time part
    const time = format(date, 'hh aa');
    return time
  }

  // Custom Event Component
  const CustomEvent = (props: any) => {
    const { event } = props;
    // @ts-ignore
    // console.log("customeevent = ", props, "activeEventModal = ", activeEventModal, "cond = ", activeEventModal && activeEventModal?.id == event?.id && event?.grouped);

    const filteredEvent = event.grouped && view == "week" ? (event.groupItems || []).find(ele => ele?.id == eventData?.id) : {};
    console.log("filteredEvent = ", filteredEvent);

    return (<>
      {event?.grouped && event?.firstGroup && view == "week" ? <>
        {/* @ts-ignore */}
        <div className={`calendarTopSection`} style={{ position: 'absolute' }}>
          <ul>
            <li className="text-[12px] py-1">{event?.title}</li>
            <li className="text-[12px] py-1">{event?.interviewer}</li>
            <li className="text-[12px] py-1">Time : {`${formatDate(event?.start)} - ${formatDate(event?.end)}`}</li>
          </ul>
        </div>
        <div className="eventbadge"><span>{event?.groupItems.length}</span></div>
      </> : (!event?.grouped || view != "week") ?
        <EventCell event={event} style={{ position: 'absolute' }} /> : null}
      {/* @ts-ignore */}
      {(activeEventModal && activeEventModal?.id == event?.id && event?.grouped) ? <EventListModel /> : (eventData?.id == event?.id || filteredEvent?.id == eventData?.id) ? <EventDetailModal /> : null}
    </>
    );
  };

  // const onRangeChange = useCallback((range: any) => {
  //   console.log('Range changed:', range);
  //   const handleCalendar = () => {
  //     const today = new Date();
  //     // @ts-ignore
  //     let monthStartDate = Array.isArray(range) ? range[0] : range?.start;
  //     // @ts-ignore
  //     let monthEndDate = Array.isArray(range) ? range[range.length - 1] : range?.end;
  //     let queryString = `from_date=${format(monthStartDate, 'yyyy-MM-dd')}&to_date=${format(monthEndDate, 'yyyy-MM-dd')}`;
  //     // @ts-ignore
  //     dispatch(getMeetingsList({ data: queryString }));
  //   }
  //   handleCalendar();
  // }, [])

  const CustomHeader = (props: any) => {
    const { label, date } = props;
    // const date = parse(label, "MMM dd", new Date());
    return (
      <div className="header">
        <div>{format(date, "dd MMM")}</div>
        <div>{format(date, "EEEE")}</div>
      </div>
    );
  };

  // console.log("eventlist = ", dashboardData, "eventMap = ", events, "eventData = ", eventData);
  console.log("selectedmonth = ", selectedMonth);

  return (
    <section className="">
      <div className="container-fluid my-md-5 my-4">
        <div className="row">
          <div className="col-lg-1 leftMenuWidth ps-0 position-relative">
            <SideMenu />
          </div>

          <div className="col-lg-11 pe-lg-4 ps-lg-0">
            <div className="row justify-content-between align-items-center">
              <div className="col-lg-8 projectText">
                <h1>Calendar</h1>
                <p className="mt-3">
                  Enjoy your selecting potential candidates Tracking and
                  Management System.
                </p>
              </div>

              <div className="col-lg-4 mt-3 mt-lg-0 text-center text-lg-end">
                <Link
                  prefetch
                  href="/P_M_JobDescriptions1"
                  className="btn btn-light me-3 mx-lg-2"
                >
                  JD Assets
                </Link>
                <Link
                  prefetch
                  href="P_M_JobDescriptions4"
                  className="btn btn-blue bg-[#0a66c2!important]"
                >
                  Create New JD
                </Link>
              </div>
            </div>

            <div className="TotalEmployees shadow bg-white rounded-3 p-3 w-100 mt-4">
              <div className="md:flex align-items-center">
                <h3 className="projectManHeading">Your Todo’s</h3>
                <div className="ml-auto d-flex todoHeadingSelect">
                  <div className="month-year-picker">
                    <select value={selectedMonth} onChange={handleMonthChange}>
                      {/* <option value="">Select Month</option> */}
                      {months.map((month) => (
                        <option key={month.value} value={month.value}>
                          {month.label}
                        </option>
                      ))}
                    </select>
                    <select value={selectedYear} onChange={handleYearChange}>
                      {/* <option value="">Select Year</option> */}
                      {years.map((year) => (
                        <option key={year.value} value={year.value}>
                          {year.label}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div
                className="d-none d-lg-block "
                style={{ width: "100%", position: "relative" }}
              >
                <div className="toolbar">
                  <div>
                    <Button
                      onClick={onPrevClick}
                      className="navigatebtn"
                    >
                      {/* <ArrowBackIosNewIcon /> */}
                      <span>{"<"}</span>
                    </Button>
                    <Button
                      onClick={onNextClick}
                      className="navigatebtn"
                    >
                      <span>{">"}</span>
                      {/* <ArrowForwardIosIcon sx={{ strokeWidth: 1 }} /> */}
                    </Button>
                  </div>
                  <div>
                    <Tabs
                      value={view}
                      // @ts-ignore
                      onChange={(e, newView) => {
                        // @ts-ignore
                        if (newView == "today") {
                          const current = new Date()
                          setDate(current);
                          setSelectedYear(getYear(current));
                          setSelectedMonth(format(current, "MM"));
                          listApiCall(new Date(), view);
                        }
                        else if (newView) {
                          setView(newView);
                          // setSelectedYear(getYear(current));
                          // setSelectedMonth(format(current, "MM"));
                          listApiCall(date, newView)
                        };
                      }}
                      className={"tabs"}
                    >
                      {calendarView.map((tb, i) => (
                        <Tab value={tb.value} label={tb.label} key={`${i}_${tb.value}`} />)
                      )}
                    </Tabs>
                  </div>
                </div>
                <Calendar
                  className="TodoDataTable"
                  dayLayoutAlgorithm={'no-overlap'}
                  selectable
                  popup={true}
                  localizer={localizer}
                  events={events}
                  startAccessor="start"
                  endAccessor="end"
                  style={{ height: 600 }}
                  date={date}
                  defaultView={"week"}
                  view={view}
                  timeslots={4} // number of per section
                  step={15}
                  views={{ month: true, week: true, day: true }} // Show only month, week, and day views
                  components={{
                    event: CustomEvent,
                    week: {
                      header: CustomHeader,
                    },
                  }}
                  formats={{
                    dayFormat: "EEEE", // day labels
                  }}
                  toolbar={false}
                  onSelectSlot={handleSelectSlot}
                  onSelectEvent={handleSelect}
                // onRangeChange={onRangeChange}
                // onView={(preview: any) => {
                //   console.log("onView = ", preview);
                // }}
                // // @ts-ignore
                // onNavigate={(newDate, newView) => {
                //   console.log("onNavigate = ", newDate, "newView = ", newView);
                //   setDate(newDate);
                //   setView(newView);
                // }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
